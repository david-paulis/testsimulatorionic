import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams ,ToastController} from 'ionic-angular';
import {TestProvider} from '../../providers/test/test';
import {Test} from '../../shared/test';
import {FormBuilder,FormGroup,Validators} from '@angular/forms';
/**
 * Generated class for the AdminPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-admin',
  templateUrl: 'admin.html',
})
export class AdminPage {
  tests:Test[];
  test:Test;
  testForm:FormGroup;
  //selectedTest=""
  constructor(public navCtrl: NavController, public navParams: NavParams,
    private testService:TestProvider,private fb:FormBuilder,private toastCtrl:ToastController) {
      this.testService.getTests().subscribe(res=>{
       this.tests=res;
       console.log(this.tests);
      },err=>console.log(err));
   this.testForm=this.fb.group({
        testId:["",Validators.required]
   });
  }
 //tests=["test 1","test 2","test 3"];
  ionViewDidLoad() {
    console.log('ionViewDidLoad AdminPage');
  }

getTest(){
  this.testService.getTest(this.testForm.value.testId).subscribe(test=>{
    this.test=test;
  },err=>console.log(err));
}
updateQ(qId,testId){
  console.log("update question ",qId);
  console.log("update test :",testId);
 this.testService.getQuestionByID(testId,qId).subscribe((res)=>
{
  this.navCtrl.push("UpdateQPage",{"question":res,"testId":testId});

},err=>console.log(err));

}
deleteQ(qId,testId){
this.testService.deleteQuestion(testId,qId).subscribe((res)=>{
  console.log(res);
  this.presentToast("Question Deleted Successfully ");
  this.testService.getTest(testId).subscribe((test)=>{
    test=this.test;
  },err=>console.log(err));
  
},err=>console.log(err));
}
updateTest(test){
 this.navCtrl.push("UpdateTestPage",{"test":test});
}
deleteTest(testId){
  this.testService.deleteTest(testId).subscribe((res)=>{
  this.presentToast("Test Deleted Successfully");
  this.navCtrl.push("AdminPage");
  },err=>console.log(err));
}
presentToast(text:string) {
  const toast = this.toastCtrl.create({
    message: text,
    duration: 3000
  });
  toast.present();
}
}
