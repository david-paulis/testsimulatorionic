import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PaymentInAppPage } from './payment-in-app';
import {InAppPurchase} from '@ionic-native/in-app-purchase';
@NgModule({
  declarations: [
    PaymentInAppPage,
  ],
  imports: [
    IonicPageModule.forChild(PaymentInAppPage),
  ],
  providers:[InAppPurchase]
})
export class PaymentInAppPageModule {}
