import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {InAppPurchase} from '@ionic-native/in-app-purchase';
/**
 * Generated class for the PaymentInAppPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-payment-in-app',
  templateUrl: 'payment-in-app.html',
})


export class PaymentInAppPage {
   TEST1="com.pmteaching.davidpaulis.test1";
   TEST2="com.pmteaching.davidpaulis.test2";
   TEST3="com.pmteaching.davidpaulis.test3";
   TEST4="com.pmteaching.davidpaulis.test4";
  productes=[];
  
  constructor(public navCtrl: NavController, public navParams: NavParams,private iap: InAppPurchase) {
   this.iap.getProducts([this.TEST1,this.TEST2,this.TEST3,this.TEST4]).then((products)=>{
     this.productes=products;
     console.log(products);
   },err=> console.log(err));
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PaymentInAppPage');
  }

   buy(productId:any){
     console.log(productId);
     //productId":"...","purchaseTime"
     this.iap.subscribe(productId).then((res)=>{
       var receiptJson=JSON.parse(res.receipt);
       console.log("productId",receiptJson.productId);
       console.log("purchaseTime",receiptJson.purchaseTime);
     }).catch(err=>console.log(err));
      
     
   }

}
