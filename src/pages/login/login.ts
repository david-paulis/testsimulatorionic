import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams , AlertController, LoadingController, Loading} from 'ionic-angular';
import {AuthProvider } from '../../providers/auth/auth';
import {TestsPage} from '../tests/tests';
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
    loading:Loading;
    loginInfo={username:'',password:''};
  constructor(public navCtrl: NavController, public navParams: NavParams ,private authService:AuthProvider
    , private alertCtrl:AlertController, private loadingCtrl:LoadingController) {
  }
 public createAccount(){
   this.navCtrl.push("RegisterPage");
 }
 public login(){
   this.authService.logIn(this.loginInfo)
         .subscribe(res=>{
           console.log(res);
           this.navCtrl.push("HomePage");
        },
          err =>{
            this.showAlert();
          });
 }
  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  showAlert() {
    const alert = this.alertCtrl.create({
      title: 'try again !',
      subTitle: 'username or password are incorrect',
      buttons: ['OK']
    });
    alert.present();
  }


}
