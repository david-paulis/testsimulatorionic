import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TestDetailsPage } from './test-details';

@NgModule({
  declarations: [
    TestDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(TestDetailsPage),
  ],
})
export class TestDetailsPageModule {}
