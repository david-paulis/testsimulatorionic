import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams ,LoadingController} from 'ionic-angular';
import { Test } from '../../shared/test';
import { AuthProvider } from '../../providers/auth/auth';
import { TestProvider } from '../../providers/test/test';
import { PaymentProvider } from '../../providers/payment/payment';
/**
 * Generated class for the TestDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
     
@IonicPage()
@Component({
  selector: 'page-test-details',
  templateUrl: 'test-details.html',
})
export class TestDetailsPage {
  test: Test;
   payed=false;
  constructor(public navCtrl: NavController, public navParams: NavParams,
    private loadingCtrl:LoadingController,
    private authService: AuthProvider, private testService: TestProvider,
    private paymentService: PaymentProvider) {
    this.test = this.navParams.get('test');
    console.log(this.test);
  }
  creatLoading(){
     var loader=this.loadingCtrl.create({
    content: "Please wait...",
    dismissOnPageChange:true
  });
  loader.present();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TestDetailsPage');
  }
  startTest(testId: string, price: number) {
     this.creatLoading();
    if (price == 0) {
      this.testService.getFreeTest(this.test._id).subscribe(test => {
        this.navCtrl.push("QuestionPage", { "test": test });
      }, err => console.log(err));

    } else {
      if (this.authService.isLoggedIn()) {
        var userId = this.authService.getUserId();
        console.log("userID", userId);
        this.paymentService.getPayedTest(userId).subscribe(res => {

          for (let pay of res) {
            if (pay.testId == this.test._id) {
              console.log(pay.createdAt);
              this.payed=true;
              this.testService.getTest(this.test._id).subscribe(test => {
                this.navCtrl.push("QuestionPage", { "test": test });
              }, err => console.log(err));
            }
          }
          if(!this.payed){
            this.navCtrl.push("AllPromotionsPage");
          }
          
        }, err => console.log(err));
      } else {
        this.navCtrl.push("LoginPage");
      }

    }


  }

}
