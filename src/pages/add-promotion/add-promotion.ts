import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams ,ToastController} from 'ionic-angular';
import { TestProvider } from '../../providers/test/test';
import { Test } from '../../shared/test';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PromotionProvider } from '../../providers/promotion/promotion';
import { promotion } from '../../shared/promotion';
import { AllPromotionsPage } from '../../pages/all-promotions/all-promotions';
/**
 * Generated class for the AddPromotionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-promotion',
  templateUrl: 'add-promotion.html',
})
export class AddPromotionPage {
  tests: Test[];
  promForm: FormGroup;

  promotion: promotion = { price: 0, title:"",test: [{ _id: "", title: "" }] };
  constructor(public navCtrl: NavController, public navParams: NavParams, private fb: FormBuilder,
    private testService: TestProvider, private promotionService: PromotionProvider,private toastCtrl:ToastController) {
    this.testService.getTests().subscribe(tests => this.tests = tests, err => console.log(err));
    this.promForm = this.fb.group({
      price: ["", Validators.required],
      test: ["", Validators.required],
      title:["",Validators.required]
    });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddPromotionPage');
  }
  add() {

    this.promotion.price = this.promForm.value.price;
    this.promotion.title = this.promForm.value.title;

    for (let key in this.promForm.value.test) {
      var test = this.promForm.value.test[key];
      var index = test.indexOf('_');
      var myId = test.slice(0, index);
      var myTitle = test.slice(index + 1, );
   
      this.promotion.test[key] = { "_id": myId, "title": myTitle };

    }

    console.log("promotion ", this.promotion);
    this.promotionService.addPromotion(this.promotion).subscribe((res) => {
      console.log(res);
      this.presentToast('Promotion was added successfully');
      this.promotion = null;
    }, err => console.log(err));
  }
  presentToast(text:string) {
    const toast = this.toastCtrl.create({
      message: text,
      duration: 3000
    });
    toast.present();
  }

}
