import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddPromotionPage } from './add-promotion';

@NgModule({
  declarations: [
    AddPromotionPage,
  ],
  imports: [
    IonicPageModule.forChild(AddPromotionPage),
  ],
})
export class AddPromotionPageModule {}
