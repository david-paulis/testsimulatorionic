import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams ,ToastController} from 'ionic-angular';
import {TestProvider} from '../../providers/test/test';
import {FormGroup, FormBuilder,Validators} from '@angular/forms';
/**
 * Generated class for the AddQPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-q',
  templateUrl: 'add-q.html',
})
export class AddQPage {
 testId:string;
 qForm:FormGroup;
  constructor(public navCtrl: NavController, public navParams: NavParams ,
   private testService:TestProvider, private fb:FormBuilder,private toastCtrl:ToastController) {
   this.testId=this.navParams.get('testId');
   this.qForm=this.fb.group({
     title: ["",Validators.required],
     answer1:["",Validators.required],
     answer2:["",Validators.required],
     answer3:["",Validators.required],
     answer4:["",Validators.required],
     explanation:["",Validators.required],
     correct:["",Validators.required],
     other:["",Validators.required],
     process:["",Validators.required]
   });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddQPage');
  }
  addQ(){
  console.log(this.testId);
  console.log(this.qForm.value);
  this.testService.addQuestion(this.testId,this.qForm.value)
  .subscribe(res=>{
    this.presentToast();
    this.qForm.reset();
  },err=>console.log(err));
  }
  presentToast() {
    const toast = this.toastCtrl.create({
      message: 'Question was added successfully',
      duration: 3000
    });
    toast.present();
  }
}
