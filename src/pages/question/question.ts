import { Component ,Pipe} from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Observable } from 'rxjs/Observable'
import 'rxjs/add/observable/timer';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/take';
import { Test, Question } from '../../shared/test';
import { TestProvider } from '../../providers/test/test';
import { Subscription } from 'rxjs/Subscription';
/**
 * Generated class for the QuestionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-question',
  templateUrl: 'question.html',

})
export class QuestionPage {
  //countDown;
  subscription: Subscription;
 // counter = .3 * 60;
  //tick = 1000;
  test: Test;
  testId: string;
  errMsg: string;
  questionIds: string[];
  prev: string;
  next: string;
  question: Question;
  answer: number;
  reviewMode = false;
  rQIds: string[];
  isPause = false;
 time;
  constructor(public navCtrl: NavController, public navParams: NavParams, private testService: TestProvider) {
    // this.testId = this.navParams.get('testId');
    // console.log("get id ",this.testId);

    this.test = this.navParams.get('test');
    console.log("get test", this.test);
    if (this.test.questions.length == 0) {

      this.errMsg = "NO Question In this Test";
      alert("NO Question In this Test");
      console.log(this.errMsg);
      return;
    }
    this.questionIds = this.test.questions.map((ques) => {
      ques.mark = false;
      return ques._id;
    });
    this.question = this.test.questions[0];
    this.setPrevNext(this.question._id);

     this.test.time=this.test.time*60;
    let timer: any = Observable.timer(0, 1000);
     this.subscription = timer.subscribe(data => {
      this.time=this.test.time-data;
      if(this.time < 1){
        this.submit();
      }

    });

  }
  ionViewDidLoad() {
   /* this.countDown = Observable.timer(500, this.tick)
      .take(this.counter)
      .map(() => {
        if (this.counter == 1) {
          return this.submit();
        } else { return --this.counter; }

      });
*/
  }
  pause(){
    this.test.time=this.time;
    this.subscription.unsubscribe();
    this.isPause=true;
  }
  resume(){
    this.isPause=false;
    let timer: any = Observable.timer(0, 1000);
     this.subscription = timer.subscribe(data => {
      this.time=this.test.time-data;
    
    });
  }


  setPrevNext(questionId: string) {
    if (this.reviewMode) {
      let index = this.rQIds.indexOf(questionId);
      this.prev = this.rQIds[(this.rQIds.length + index - 1) % this.rQIds.length];
      this.next = this.rQIds[(this.rQIds.length + index + 1) % this.rQIds.length];

    } else {
      let index = this.questionIds.indexOf(questionId);
      this.prev = this.questionIds[(this.questionIds.length + index - 1) % this.questionIds.length];
      this.next = this.questionIds[(this.questionIds.length + index + 1) % this.questionIds.length];
    }

  }
  getQuestionById(id: string) {

    this.question = this.test.questions.filter((ques) => { return ques._id == id; })[0];
    this.setPrevNext(this.question._id);
  }
  getnext() {
    console.log(this.answer);
    this.question.answer = this.answer;
    this.getQuestionById(this.next);
    this.answer = this.question.answer || -1;


  }
  getprev() {
    console.log(this.answer);
    this.question.answer = this.answer;
    this.getQuestionById(this.prev);
    this.answer = this.question.answer || -1;

  }
  mark(id: string) {
    if (this.rQIds) {
      this.rQIds.push(id);
    } else {
      this.rQIds = [""];
      this.rQIds[0] = id;
    }

    this.question.mark = true;

  }
  unMark(id: string) {
    var index = this.rQIds.indexOf(id);
    if (index != -1) {
      this.rQIds.splice(index, 1);
    }
    this.question.mark = false;
  }
  review() {
    var rQuestion = this.test.questions.filter((ques) => { return ques.mark == true });
    if (rQuestion.length < 1) { return }
    this.reviewMode = true;
    this.question = rQuestion[0];
    this.setPrevNext(this.question._id);

  }
  exitReview() {
    this.reviewMode = false;
    this.question = this.test.questions[0];
    this.setPrevNext(this.question._id);
  }
  submit() {
    this.subscription.unsubscribe();
    console.log("will submit :", this.test);
    this.navCtrl.push("ResultPage", { "test": this.test });
  }



}

