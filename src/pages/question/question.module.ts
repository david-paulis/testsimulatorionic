import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { QuestionPage } from './question';
import {PipesModule} from '../../pipes/pipes.module';
@NgModule({
  declarations: [
    QuestionPage,
    
    
  ],
  imports: [
    IonicPageModule.forChild(QuestionPage),
    PipesModule
  ],
})
export class QuestionPageModule {}
