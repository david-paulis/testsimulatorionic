import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams ,LoadingController } from 'ionic-angular';
import {TestProvider} from '../../providers/test/test';
import {Test} from '../../shared/test';
/**
 * Generated class for the TestsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tests',
  templateUrl: 'tests.html',
})
export class TestsPage {
 tests:Test[];
 errMsg:string;

  constructor(public navCtrl: NavController,private testService:TestProvider,
  private loadingCtrl:LoadingController) {
    this.creatLoader();
    this.testService.getTests().subscribe(tests =>{
      if(tests !=null){
        this.tests = tests;    
       
      }

    },err =>{this.errMsg= err; alert(err);} );
  }
  
 creatLoader(){
 var loader=this.loadingCtrl.create({
    content: "Please wait...",
    dismissOnPageChange:true
  });
 loader.present();
 }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TestsPage');
  }
  open(test:Test){
    console.log(test);
    this.navCtrl.push("TestDetailsPage" , { test: test});
  }
 
}
