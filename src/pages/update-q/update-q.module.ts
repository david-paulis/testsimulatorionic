import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UpdateQPage } from './update-q';

@NgModule({
  declarations: [
    UpdateQPage,
  ],
  imports: [
    IonicPageModule.forChild(UpdateQPage),
  ],
})
export class UpdateQPageModule {}
