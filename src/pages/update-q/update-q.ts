import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ToastController } from 'ionic-angular';
import {TestProvider} from '../../providers/test/test';
import {FormGroup, FormBuilder,Validators} from '@angular/forms';
import {Question} from '../../shared/test';
/**
 * Generated class for the AddQPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-update-q',
  templateUrl: 'update-q.html',
})
export class UpdateQPage {
 testId:string;
 question:Question;
 qForm:FormGroup;
  constructor(public navCtrl: NavController, public navParams: NavParams ,
   private testService:TestProvider, private fb:FormBuilder,private toastCtrl:ToastController) {
   this.testId=this.navParams.get('testId');
   this.question=this.navParams.get('question');
   console.log("testId",this.testId);
   console.log("question",this.question)
   this.qForm=this.fb.group({
     title: ["",Validators.required],
     answer1:["",Validators.required],
     answer2:["",Validators.required],
     answer3:["",Validators.required],
     answer4:["",Validators.required],
     explanation:["",Validators.required],
     correct:["",Validators.required],
     other:["",Validators.required],
     process:["",Validators.required]
   });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddQPage');
  }
  
  updateQ(){
    this.testService.updateQuestion(this.testId,this.question._id,this.qForm.value)
      .subscribe(res=>{
       console.log("res ",res);
       this.presentToast("Question Updated Successfully");
       this.navCtrl.push("AdminPage");
      },err=>console.log(err));
  }
  presentToast(text:string) {
    const toast = this.toastCtrl.create({
      message: text,
      duration: 3000
    });
    toast.present();
  }
}
