import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams ,ToastController} from 'ionic-angular';
import {TestProvider} from '../../providers/test/test';
import {FormGroup, FormBuilder , Validators} from '@angular/forms';
/**
 * Generated class for the AddTestPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-test',
  templateUrl: 'add-test.html',
})
export class AddTestPage {
 addForm:FormGroup;
  constructor(public navCtrl: NavController, public navParams: NavParams,
    private testService:TestProvider,private fb:FormBuilder,private toastCtrl:ToastController) {
      this.addForm = this.fb.group({
        title:["",Validators.required],
        description:["",Validators.required],
        time:["",Validators.required],
        price:["",Validators.required],
        numQ:["",Validators.required]
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddTestPage');
  }
 addTest(){
  this.testService.addTest(this.addForm.value).subscribe((res)=>{
    console.log("test id",res._id);
    this.presentToast();
   this.navCtrl.push("AddQPage",{"testId":res._id});
  },err=>console.log(err));
 }

 presentToast() {
  const toast = this.toastCtrl.create({
    message: 'Test was added successfully',
    duration: 3000
  });
  toast.present();
}
}
