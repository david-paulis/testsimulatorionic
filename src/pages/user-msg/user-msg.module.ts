import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UserMsgPage } from './user-msg';

@NgModule({
  declarations: [
    UserMsgPage,
  ],
  imports: [
    IonicPageModule.forChild(UserMsgPage),
  ],
})
export class UserMsgPageModule {}
