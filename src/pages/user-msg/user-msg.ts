import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams ,AlertController} from 'ionic-angular';
import {AuthProvider} from '../../providers/auth/auth';
import {EmailComposer} from '@ionic-native/email-composer';

/**
 * Generated class for the UserMsgPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-user-msg',
  templateUrl: 'user-msg.html',
  providers:[EmailComposer]
})
export class UserMsgPage {
 users:any[];
 emails:string[];
  constructor(public navCtrl: NavController, public navParams: NavParams
   ,private authService:AuthProvider,private emailComposer:EmailComposer, private alertCtrl:AlertController) {
     this.authService.getAlluser().subscribe(users =>this.users=users
      ,err=> console.log(err));
  
      this.authService.getEmails().subscribe(email=>{this.emails=email;
       console.log(this.emails)}
     ,err=> console.log(err));
 
    }
    
  ionViewDidLoad() {
    console.log('ionViewDidLoad UserMsgPage');
  }
  sendEmailToAll(){
     
this.emailComposer.isAvailable().then((available: boolean) =>{
  if(available) {
    //Now we know we can send
    let email = {
      to: this.emails,

      attachments: [
        
      ],

      isHtml: true
    };
    
    // Send a text message using default options
    this.emailComposer.open(email);
  }else{
    this.showAlert();
  }
 });
 

 
  }
  sendToUser(userEmail:string){
   alert("send email to : "+userEmail);
    this.emailComposer.isAvailable().then((available: boolean) =>{
      if(available) {
        //Now we know we can send
        let email = {
          to: userEmail,
    
          attachments: [
            
          ],
    
          isHtml: true
        };
        
        // Send a text message using default options
        this.emailComposer.open(email).then((res)=>{
         alert("response :"+res);
        },err=>alert(err));
      }
      else{
      this.showAlert();
      }
     });
     

}
showAlert() {
  const alert = this.alertCtrl.create({
    title: 'Email App Not Found!',
    subTitle: 'Please Install any email application',
    buttons: ['OK']
  });
  alert.present();
}
}
