import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams ,AlertController} from 'ionic-angular';
import {AuthProvider} from '../../providers/auth/auth';

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
  errMsg:string;
  registerInfo = { firstname: '', email: '',username:'', password: '',lastname:"" ,confirmation_password: '' };
  constructor(public navCtrl: NavController, public navParams: NavParams,private alertCtrl: AlertController
    ,private authService:AuthProvider) {
  }
 
  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }
  public register() {
     console.log(this.registerInfo);
    this.authService.signUp(this.registerInfo)
                  .subscribe((res)=>{
                    console.log("response",res);
                    this.navCtrl.push("LoginPage");
                  },err=> {console.log(err); this.showAlert();});
          
  }

  showAlert() {
    const alert = this.alertCtrl.create({
      title: 'username already used !',
      subTitle: 'please , Choose another username',
      buttons: ['OK']
    });
    alert.present();
  }

}
