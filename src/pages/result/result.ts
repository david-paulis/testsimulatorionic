import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Test, Question } from '../../shared/test';

/**
 * Generated class for the ResultPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-result',
  templateUrl: 'result.html',
})
export class ResultPage {
  res: string = "result";
  test: Test;
  question: Question;
  result = {
    "total": 0,
    "correct": 0,
    "p1": 0,
    "p2": 0,
    "p3": 0,
    "p4": 0,
    "p5": 0,
    "tp1": 0,
    "tp2": 0,
    "tp3": 0,
    "tp4": 0,
    "tp5": 0
  };
  selectedAnswer:string;//to show in detail result page
  finalResult: string;
  process1: string;
  process2: string;
  process3: string;
  process4: string;
  process5: string;
  i=0;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.test = this.navParams.get('test');
    console.log("get Test", this.test);
    this.preCalResult();
    this.calResutl();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ResultPage');
  }
  preCalResult() {
    this.result.total = this.test.questions.length;
    for (let question of this.test.questions) {
      if (question.process == 1) {
        this.result.tp1++;
        if (question.answer == question.correct) {
          this.result.correct++;
          this.result.p1++;
        }
      } else if (question.process == 2) {
        this.result.tp2++;
        if (question.answer == question.correct) {
          this.result.correct++;
          this.result.p2++;
        }

      } else if (question.process == 3) {
        this.result.tp3++;
        if (question.answer == question.correct) {
          this.result.correct++;
          this.result.p3++;
        }

      } else if (question.process == 4) {
        this.result.tp4++;
        if (question.answer == question.correct) {
          this.result.correct++;
          this.result.p4++;
        }

      } else if (question.process == 5) {
        this.result.tp5++;
        if (question.answer == question.correct) {
          this.result.correct++;
          this.result.p5++;
        }

      } else {
        if (question.answer == question.correct) {
          this.result.correct++;
        }
      }
    }
  }
  calResutl() {

    this.finalResult = ((this.result.correct / this.result.total) * 100).toFixed(2);

    this.process1 = ((this.result.p1 / this.result.tp1) * 100).toFixed(2);

    this.process2 = ((this.result.p2 / this.result.tp2) * 100).toFixed(2);


    this.process3 = ((this.result.p3 / this.result.tp3) * 100).toFixed(2);


    this.process4 = ((this.result.p4 / this.result.tp4) * 100).toFixed(2);

    this.process5 = ((this.result.p5 / this.result.tp5) * 100).toFixed(2);

    console.log("final result :", this.finalResult);
  }
  getQuestion(question:Question) {
         console.log("get q ",question);
         var  answer=question.answer;
       if(answer==1){
         this.selectedAnswer ="You Select First Option"
       }else if(answer ==2){
        this.selectedAnswer ="You Select Second Option"
      }else if(answer ==3){
        this.selectedAnswer ="You Select Third Option"

       }else if(answer ==4){
        this.selectedAnswer ="You Select Fourth Option"

       }else {
        this.selectedAnswer ="You Leave This Question"

       }
         this.question=question;

  }
}
