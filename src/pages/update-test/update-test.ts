import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ToastController } from 'ionic-angular';
import {TestProvider} from '../../providers/test/test';
import {FormGroup, FormBuilder , Validators} from '@angular/forms';
import {Test} from '../../shared/test';
/**
 * Generated class for the AddTestPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-update-test',
  templateUrl: 'update-test.html',
})
export class UpdateTestPage {
 addForm:FormGroup;
 test:Test;
  constructor(public navCtrl: NavController, public navParams: NavParams,
    private testService:TestProvider,private fb:FormBuilder,private toastCtrl:ToastController) {
      this.test=this.navParams.get('test');
      console.log(this.test);
      this.addForm = this.fb.group({
        title:["",Validators.required],
        description:["",Validators.required],
        time:["",Validators.required],
        price:["",Validators.required],
        numQ:["",Validators.required]
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddTestPage');
  }
 updateTest(){
 this.testService.updateTest(this.addForm.value,this.test._id)
 .subscribe((res)=>{
 this.presentToast("Update Test Successfully");
 this.navCtrl.push("AdminPage");
 },err=>console.log(err));
 }
 presentToast(text:string) {
  const toast = this.toastCtrl.create({
    message: text,
    duration: 3000
  });
  toast.present();
}
}
