import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PayPal, PayPalPayment, PayPalConfiguration } from '@ionic-native/paypal';
import {Test} from '../../shared/test';
import {promotion} from '../../shared/promotion';
/**
 * Generated class for the PaymentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-payment',
  templateUrl: 'payment.html',
})
export class PaymentPage {
  test:Test;
  promo:promotion;
	currencies = ['EUR', 'USD'];
	payPalEnvironment: string = 'payPalEnvironmentSandbox';
  constructor(public navCtrl: NavController, public navParams: NavParams , private payPal:PayPal) {
   if(this.navParams.get('test')){
     this.test=this.navParams.get('test');
     this.makePayment(this.test.price,this.test.title);
   }else if(this.navParams.get('promo')){
     this.promo=this.navParams.get('promo');
     this.makePayment(this.promo.price,this.promo.title);
   }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PaymentPage');
  }


 

	makePayment(price:any,title:any) {
  var  payment: PayPalPayment = new PayPalPayment(price, 'USD',title, 'sale');

		this.payPal.init({
			PayPalEnvironmentProduction:"",
			PayPalEnvironmentSandbox: "ARCLHc0QJVr80lHNsGfVb983KMOyRoVGFrFvMJUH-musuQme6nDY7X3eKDkyry495RaqaEJJ38yZVhEp"
		}).then(() => {
			this.payPal.prepareToRender(this.payPalEnvironment, new PayPalConfiguration({})).then(() => {
				this.payPal.renderSinglePaymentUI(payment).then((response) => {
					alert(`Successfully paid. Status = ${response.response.state}`);
					console.log(response);
				}, () => {
					console.error('Error or render dialog closed without being successful');
				});
			}, () => {
				console.error('Error in configuration');
			});
		}, () => {
			console.error('Error in initialization, maybe PayPal isn\'t supported or something else');
		});
	}
}
