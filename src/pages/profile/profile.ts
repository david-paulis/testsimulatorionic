import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {AuthProvider} from '../../providers/auth/auth';
import {User} from '../../shared/user';
import {PaymentProvider} from '../../providers/payment/payment';
import {Payment} from '../../shared/payment';
/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {
 admin:boolean;
 user:any;
 payments:Payment[];
  constructor(public navCtrl: NavController, public navParams: NavParams,
    private authService:AuthProvider,private paymentService:PaymentProvider) {
      if(!this.authService.isLoggedIn())
      {
        this.navCtrl.push("LoginPage");
      }else {
        this.admin=  this.authService.isAdmin();
        this.user= this.authService.getUser();
    console.log("admin",this.admin);
    console.log("user",this.user);
   // if(!this.admin){
     this.paymentService.getPayedTest(this.user._id).subscribe((payment)=>
   {
     console.log(payment);
     this.payments=payment;
   },err=>console.log(err));
      }
  
// }
    

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');
  }
  addTest(){
    this.navCtrl.push("AddTestPage");
  }
  addSale(){
    this.navCtrl.push("AddPromotionPage");
  }
  sendEmail(){
    this.navCtrl.push("UserMsgPage");
  }
  manageTest(){
    this.navCtrl.push("AdminPage");
  }
  managePromotion(){
    this.navCtrl.push("AllPromotionsPage");
  }
}
