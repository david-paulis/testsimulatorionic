import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AllPromotionsPage } from './all-promotions';

@NgModule({
  declarations: [
    AllPromotionsPage,
  ],
  imports: [
    IonicPageModule.forChild(AllPromotionsPage),
  ],
})
export class AllPromotionsPageModule {}
