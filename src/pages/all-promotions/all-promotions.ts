import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams ,AlertController,ToastController} from 'ionic-angular';
import { PromotionProvider } from '../../providers/promotion/promotion';
import { promotion } from '../../shared/promotion';
import { AuthProvider } from '../../providers/auth/auth';
import { Test } from '../../shared/test';
import { TestProvider } from '../../providers/test/test';
import { PayPal, PayPalPayment, PayPalConfiguration } from '@ionic-native/paypal';

/**
 * Generated class for the AllPromotionsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-all-promotions',
  templateUrl: 'all-promotions.html',
})

export class AllPromotionsPage {
  testId="";
  promotions: promotion[];
  admin: boolean = false;
  tests: Test[];
  selectedPromo:promotion;
  selectedTest:Test;
  currencies = ['EUR', 'USD'];
	payPalEnvironment: string = 'payPalEnvironmentSandbox';
  constructor(public navCtrl: NavController, public navParams: NavParams, private testService: TestProvider,
    private toastCtrl:ToastController,private payPal:PayPal,
    private promotionService: PromotionProvider, private authService: AuthProvider,private alertCtrl: AlertController) {
    this.promotionService.getPromotions().subscribe(promo => {
      this.promotions = promo;
      console.log(this.promotions);
      this.admin = this.authService.isAdmin();
    }, err => console.log(err));
    this.testService.getTests().subscribe((tests) => {
      this.tests = tests;
    },err=>console.log(err));

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AllPromotionsPage');
  }
  delete(id: string) {
    if (this.authService.isAdmin()) {
      this.promotionService.deletePromotion(id).subscribe(res => {

        this.promotions = res;
      }, err => console.log(err));
    } else {
      console.log("not allowed to delete !");
    }

  }
buy(test:Test){
  this.selectedTest=test;
  console.log("buy test :",test);
  var mes=`Title  ${test.title} , Price ${test.price} USD`;
this.presentConfirm(mes);
}
buyPromotion(promotion:promotion){
  this.selectedPromo=promotion;
  console.log("buy promotion ",promotion);
  var mes=`Title  ${promotion.title} , Price  ${promotion.price} USD`;
  this.presentConfirm(mes);
}

makePayment(price:any,title:any) {
  var  payment: PayPalPayment = new PayPalPayment(price, 'USD',title, 'sale');

		this.payPal.init({
			PayPalEnvironmentProduction:"",
			PayPalEnvironmentSandbox: "AedsuuSimA7pW62sbpIHJxeTdxQiJoRDWQVjG2nqB7SJk4vwbpSr7e3YJG5tbQMF8QYMT06qNMK9lDu1"
		}).then(() => {
			this.payPal.prepareToRender(this.payPalEnvironment, new PayPalConfiguration({})).then(() => {
				this.payPal.renderSinglePaymentUI(payment).then((response) => {
          alert(`Successfully paid. Status = ${response.response.state}`);
          alert(response);
          alert(response.response);
					console.log(response);
				}, () => {
					console.error('Error or render dialog closed without being successful');
				});
			}, () => {
				console.error('Error in configuration');
			});
		}, () => {
			console.error('Error in initialization, maybe PayPal isn\'t supported or something else');
		});
	}
presentConfirm(test:any) {
  let alert = this.alertCtrl.create({
    title: 'Do you want to buy this ?',
    message: test,
    buttons: [
      {
        text: 'Cancel',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      },
      {
        text: 'Buy',
        handler: () => {
          console.log('Buy clicked');
         // this.navCtrl.push("PaymentPage",{"test":this.selectedTest,"promo":this.selectedPromo});
         if(this.selectedPromo){
            this.makePayment(this.selectedPromo.price,this.selectedPromo.title);
         }else if(this.selectedTest){
            this.makePayment(this.selectedTest.price,this.selectedTest.title);
         }
        
        }
      }
    ]
  });
  alert.present();
}
presentToast(text:string) {
  const toast = this.toastCtrl.create({
    message: text,
    duration: 3000
  });
  toast.present();
}
}
