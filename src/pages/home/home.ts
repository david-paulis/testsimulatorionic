import { Component } from '@angular/core';
import { NavController, IonicPage } from 'ionic-angular';
import { Subscription } from 'rxjs/Subscription';
import { AuthProvider } from '../../providers/auth/auth';
import { OnInit, OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { AlertController } from 'ionic-angular';

import { Network } from '@ionic-native/network';
@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements OnInit, OnDestroy {
  username: string = undefined;
  subscription: Subscription;
  errMsg: string;
  constructor(public navCtrl: NavController, private alertCtrl: AlertController
    , private authService: AuthProvider, private network: Network) {
    this.authService.loadUserCredentials();
    this.subscription = this.authService.getUsername()
      .subscribe(name => { console.log(name); this.username = name; });
      //when disconnect
    let disconnectSubscription = this.network.onDisconnect().subscribe(() => {

    });
    //when connect to internet
    let connectSubscription = this.network.onConnect().subscribe(() => {
      console.log('network connected!');
      
      this.authService.loadUserCredentials();
      this.subscription = this.authService.getUsername()
        .subscribe(name => { console.log(name); this.username = name; });


    });  
  }          
  ngOnInit() {
    console.log("start home view");
  }
  ngOnDestroy() {
   this.subscription.unsubscribe();
  }   
  logOut() {
    this.username = undefined;
    this.authService.logOut();
  }
  start() {      
   // this.navCtrl.push("TestsPage");
    if (this.network.type != "none") {
      


      this.navCtrl.push("TestsPage");
    }
    else {
      this.showAlert("Internet Access !", "Please , Connect to the internet to continue ");
    }
  }

  showAlert(title: string, subtitle: string) {
    const alert = this.alertCtrl.create({
      title: title,
      subTitle: subtitle,
      buttons: ['OK']
    });
    alert.present();
  }

}
