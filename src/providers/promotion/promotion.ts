import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {baseURL} from '../../shared/baseUrl';
import {Observable} from 'rxjs/Observable';
import {ProcessHttpMsgProvider} from '../process-http-msg/process-http-msg';
import {promotion} from '../../shared/promotion';
/*
  Generated class for the PromotionProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class PromotionProvider {

  constructor(public http: HttpClient,private processHTTPMsg:ProcessHttpMsgProvider) {
    console.log('Hello PromotionProvider Provider');
  }

 addPromotion(promotion:any):Observable<any>{
  return this.http.post(baseURL+'promotion',promotion)
        .catch(err=>{return this.processHTTPMsg.handleError(err);});
}
deletePromotion(promotionId:string):Observable<any>{
  return this.http.delete(baseURL+'promotion/'+promotionId)
  .catch(err=>{return this.processHTTPMsg.handleError(err);});

}
getPromotions():Observable<promotion[]>{
  return this.http.get(baseURL+'promotion')
  .catch(err=>{return this.processHTTPMsg.handleError(err);});

}
}
