import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {Observable } from 'rxjs/Observable';
import {Subject} from 'rxjs/Subject';
import {baseURL} from '../../shared/baseUrl';
import 'rxjs/add/operator/catch';
import {ProcessHttpMsgProvider} from '../process-http-msg/process-http-msg';
import { Testability } from '@angular/core/src/testability/testability';
import {Payment} from '../../shared/payment';
/*
  Generated class for the PaymentProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class PaymentProvider {

  constructor(public http: HttpClient,private processHTTPMsg:ProcessHttpMsgProvider) {
    console.log('Hello PaymentProvider Provider');
  }
  getPayedTest(userId:string):Observable<Payment[]>{
    return this.http.get(baseURL+'payment/'+userId)
    .catch(err=>{return this.processHTTPMsg.handleError(err);});
  }
  postPayment(userId:string,testId:string):Observable<any>{
    return this.http.post(baseURL+'payment',{'userId':userId,'testId':testId})
    .catch(err=>{return this.processHTTPMsg.handleError(err);});

  }

}
