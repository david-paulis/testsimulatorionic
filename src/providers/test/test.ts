import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {baseURL} from '../../shared/baseUrl';
import {Observable} from 'rxjs/Observable';
import {ProcessHttpMsgProvider} from '../process-http-msg/process-http-msg';
import {Test,Question} from '../../shared/test';
/*
  Generated class for the TestProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class TestProvider {

  constructor(public http: HttpClient,private processHTTPMsg:ProcessHttpMsgProvider) {
    console.log('Hello TestProvider Provider');
  }
  addTest(test:any):Observable<any>{
    return this.http.post(baseURL+'test',test)
                    .catch(err=>{return this.processHTTPMsg.handleError(err);});
  }
  updateTest(test:any,testId:string):Observable<any>{
    return this.http.put(baseURL+'test/'+testId,test)
                .catch(err=>{ return this.processHTTPMsg.handleError(err);});
  }
  deleteTest(id:string):Observable<any>{
   return this.http.delete(baseURL+'test/'+id)
            .catch(err=>{return this.processHTTPMsg.handleError(err);});
  }
  getTests():Observable<Test[]>{
    return this.http.get(baseURL+'test')
    .catch(err=>{return this.processHTTPMsg.handleError(err);});
  }
  getFreeTest(id:string):Observable<Test>{
    return this.http.get(baseURL+'test/free/'+id)
            .catch(err=>{return this.processHTTPMsg.handleError(err);});
  }
  getTest(testID:string):Observable<Test>{
    return this.http.get(baseURL+'test/'+testID)
        .catch(err=>{return this.processHTTPMsg.handleError(err);});

  }
  addQuestion(testID:string ,q:any){
      return this.http.post(baseURL+'test/'+testID+'/question',q)
         .catch(err=>{return this.processHTTPMsg.handleError(err);});
    }
  deleteQuestion(testID:string,questionID:string):Observable<any>{
  return  this.http.delete(baseURL+'test/'+testID+'/question/'+questionID)
         .catch(err=>{return this.processHTTPMsg.handleError(err);});

  }
  updateQuestion(testID:string,questionID:string, question:any):Observable<any>{
    return  this.http.put(baseURL+'test/'+testID+'/question/'+questionID,question)
      .catch(err=>{return this.processHTTPMsg.handleError(err);});

  }
  getAllQuestion(testID:string):Observable<any>{
    return this.http.get(baseURL+'test/'+testID+'/question')
    .catch(err=>{return this.processHTTPMsg.handleError(err);});

  }
  getQuestionByID(testID:string,questionID:string):Observable<any>{
    return this.http.get(baseURL+'test/'+testID+'/question/'+questionID)
       .catch(err=>{return this.processHTTPMsg.handleError(err);});
   
  }    


}
