import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {Observable } from 'rxjs/Observable';
import {Subject} from 'rxjs/Subject';
import { Storage } from '@ionic/storage';
import {baseURL} from '../../shared/baseUrl';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import {ProcessHttpMsgProvider} from '../process-http-msg/process-http-msg';
import {User} from '../../shared/user';
/*
  Generated class for the AuthProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
interface AuthResponse{
    status: string,
    success: string,
    token:string
};
interface JWTResponse {
  status:string,
  success:string,
  user:any
};

@Injectable()
export class AuthProvider {
 tokenKey: string ='JWT';
 isAuthenticated:Boolean =false;
 username: Subject<string> = new Subject<string>();
 authToken:string=undefined;
 userId:string=undefined;
 admin:boolean=false;
 user:any;
  constructor(public http: HttpClient,private localStorage:Storage
    ,private processHTTPMsgService:ProcessHttpMsgProvider) {
  }
  checkJWTtoken() {
    this.http.get<JWTResponse>(baseURL + 'users/checkJWTtoken')
    .subscribe(res => {
      console.log("JWT Token Valid: ", res);
      this.userId=res.user._id;
      this.admin=res.user.admin;
      this.user=res.user;
      this.sendUsername(res.user.username);
    },
    err => {
      console.log("JWT Token invalid: ", err);
      this.destroyUserCredentials();
    })     
  }
  getUser(){
    return this.user;
  }
 
  sendUsername(name: string) {
    this.username.next(name);
  }

  clearUsername() {
    this.username.next(undefined);
  }

  loadUserCredentials() {
    var credentials = JSON.parse(localStorage.getItem(this.tokenKey));
    console.log("loadUserCredentials ", credentials);
    if (credentials && credentials.username != undefined) {
      this.useCredentials(credentials);
      if (this.authToken)
        this.checkJWTtoken();
    }
  }

  storeUserCredentials(credentials: any) {
    console.log("storeUserCredentials ", credentials);    
    localStorage.setItem(this.tokenKey, JSON.stringify(credentials));
    this.useCredentials(credentials);
  }

  useCredentials(credentials: any) {
    this.isAuthenticated = true;
    this.sendUsername(credentials.username);
    this.authToken = credentials.token;
  }

  destroyUserCredentials() {
    this.authToken = undefined;
    this.clearUsername();
    this.isAuthenticated = false;
    localStorage.removeItem(this.tokenKey);
  }

  signUp(user:any):Observable<any> {
    console.log(baseURL + 'users/signup');
    console.log("user",user);
  return this.http.post<AuthResponse>(baseURL + 'users/signup',user)
  .catch(error => { return this.processHTTPMsgService.handleError(error); });

  }

  logIn(user: any): Observable<any> {
    return this.http.post<AuthResponse>(baseURL + 'users/login', 
      {"username": user.username, "password": user.password})
      .map(res => {
          this.storeUserCredentials({username: user.username, token: res.token});

          return {'success': true, 'username': user.username };
      })
        .catch(error => { return this.processHTTPMsgService.handleError(error); });
  }
  
 /* logIn(user: any): Observable<any> {
    return this.http.post<AuthResponse>(baseURL + 'users/login', 
      {"username": user.username, "password": user.password})
        .catch(error => { return this.processHTTPMsgService.handleError(error); });
  }*/

  logOut() {
    this.destroyUserCredentials();
    this.userId=undefined;
  }

  isLoggedIn(): Boolean {
    return this.isAuthenticated;
  }

  getUsername(): Observable<string> {
    return this.username.asObservable();
  }

  getToken(): string {
    return this.authToken;
  }
  getUserId():string{
    return this.userId;
  }
  getAlluser():Observable<any>{
    return this.http.get(baseURL+'users')
      .catch(error => { return this.processHTTPMsgService.handleError(error); });
  }
  getEmails():Observable<string[]>{
    return this.http.get(baseURL+'users/emails')
    .catch(error => { return this.processHTTPMsgService.handleError(error); });

  }
  isAdmin():boolean{
  return this.admin;
  }
}


     