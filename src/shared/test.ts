
export interface Question {
    _id: string,
    process: number,
    other: number,
    answer1: string,
    answer2: string,
    answer3: string,
    answer4: string,
    correct: number,
    title: string,
    explanation: string,
    answer :number,
    mark:boolean
};
export interface Test {
    _id: string,
    price: number,
    title: string,
    description: string,
    time: number,
    questions: Question[]
    
};