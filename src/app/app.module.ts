import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { MyApp } from './app.component';
import { StatusBar } from '@ionic-native/status-bar';
import { AuthProvider } from '../providers/auth/auth';
import { IonicStorageModule } from '@ionic/storage';
import { ProcessHttpMsgProvider } from '../providers/process-http-msg/process-http-msg';
import { TestProvider } from '../providers/test/test';
import {PayPal} from '@ionic-native/paypal';
import { HttpClientModule ,HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthInterceptorProvider, UnauthorizedInterceptor } from '../providers/auth-interceptor/auth-interceptor';
import { PromotionProvider } from '../providers/promotion/promotion';
import { ReactiveFormsModule } from '@angular/forms';
import { PaymentProvider } from '../providers/payment/payment';
import { Network } from '@ionic-native/network';
import { SplashScreen } from '@ionic-native/splash-screen';
import {LaunchReview} from '@ionic-native/launch-review';
@NgModule({ 
  declarations: [
    MyApp
   
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot({
      name: '__pmpdb',
      driverOrder: ['indexeddb', 'sqlite', 'websql']
    }),
    HttpClientModule,
    ReactiveFormsModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
   
  ],
  providers: [
    StatusBar,
    SplashScreen,
    LaunchReview,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthProvider,
    ProcessHttpMsgProvider,
    AuthInterceptorProvider,
    TestProvider,
    PayPal,
    Network,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptorProvider,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: UnauthorizedInterceptor,
      multi: true
},
    PromotionProvider,
    PaymentProvider
  ]
})
export class AppModule {}
