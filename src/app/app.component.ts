import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { LaunchReview } from '@ionic-native/launch-review';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: string = "HomePage";

  pages: Array<{title: string, component: any,icon:string}>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen,
  private launchReview:LaunchReview) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', component: "HomePage" ,icon:"home"},
      { title: 'My Profile', component: "ProfilePage" ,icon:"person"},
      { title:'About Me', component:"AboutmePage",icon:"information-circle" },
      { title :"Login", component:"LoginPage",icon:"log-in"},
      {title:"Payment",component:"PaymentInAppPage",icon:"card"}
    //  <ion-icon name="home"></ion-icon>


    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
  review(){
    this.launchReview.launch()
    .then(() => 
    {console.log('Successfully launched store app ');});
    if(this.launchReview.isRatingSupported()){
     this.launchReview.rating()
       .then(() => console.log('Successfully launched rating dialog'));
   }
  }
}
